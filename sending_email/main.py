import smtplib
from email.message import EmailMessage
from string import Template
from pathlib import Path

html = Template(Path("index.html").read_text())

email = EmailMessage()
email['from'] = "your_name"
email['to'] = "your_dest_mail"
email['subject'] = "**********"
email.set_content(html.substitute(name = "JDK"), 'html')

with smtplib.SMTP(host='smtp.gmail.com', port=587) as smtp:
    smtp.starttls()
    smtp.ehlo()
    smtp.login("your_mail", "your_password")
    smtp.send_message(email)
    print("Message sent !")
