
import os
#pip3 install twilio
from twilio.rest import Client 


# Find your Account SID and Auth Token at twilio.com/console
# and set the environment variables
account_sid = os.environ['TWILIO_ACCOUNT_SID']
auth_token = os.environ['TWILIO_AUTH_TOKEN']

client = Client(account_sid, auth_token)

message = client.messages \
                .create(
                     body="This is a program to send SMS using Python",
                     from_='+17579066716', # get this numb on the documentation 
                     to='+225********' # your verified number 
                 )

print(message.sid)



# 82Wkg2KBmgZcfqyT5
