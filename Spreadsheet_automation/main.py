import openpyxl

inv_file = openpyxl.load_workbook("inventory.xlsx")
product_list = inv_file["Product1"]

product_per_supplier = {}
total_value_per_supplier = {}
product_under_10_inv = {}

for product_row in range(2, product_list.max_row + 1):
    supplier_name = product_list.cell(product_row, 4).value
    inventory = product_list.cell(product_row, 2).value
    price = product_list.cell(product_row, 3).value
    product = product_list.cell(product_row, 1).value
    inventory_price = product_list.cell(product_row, 5)

    # calcul du nombre de clients
    if supplier_name in product_per_supplier:
        product_per_supplier[supplier_name] = product_per_supplier[supplier_name] + 1
    else:
        product_per_supplier[supplier_name] = 1

    # calcul des revenus par clients
    if supplier_name in total_value_per_supplier:
        total_value_per_supplier[supplier_name] = total_value_per_supplier[supplier_name] + inventory * price
    else:
        total_value_per_supplier[supplier_name] = inventory * price

    # liste des produits en dessous de 10
    if inventory < 10:
        product_under_10_inv[product] = inventory

    # Calcul de l'inventaire
    inventory_price.value = inventory * price

inv_file.save("inventory_with_inventory_value.xlsx")

print(product_per_supplier)
print(total_value_per_supplier)
print(product_under_10_inv)





