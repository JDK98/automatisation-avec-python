from datetime import datetime

user_input = input("Enter your goal and the deadline: \n")
user_input_list = user_input.split(":")

goal = user_input_list[0]
deadline = user_input_list[1]

deadline_date = datetime.strptime(deadline, "%d.%m.%Y")
today_date = datetime.today()

days_left = deadline_date - today_date
print(f"Dear user, the time remaining till your goal {goal} is {days_left.days} days")
