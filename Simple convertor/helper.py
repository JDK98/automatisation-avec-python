def convert_function(number_of_days_int, unit):
    if unit == "hours":
        return f"{number_of_days_int} days equal to {number_of_days_int * 24} hours"
    elif unit == "minutes":
        return f"{number_of_days_int} days equal to {number_of_days_int * 24 * 60} minutes"
    elif unit == "secondes":
        return f"{number_of_days_int} days equal to {number_of_days_int * 24 * 60 * 60} secondes"
    else:
        return f"you have to enter a correct unit"

def validation(number_of_days_dict):
    try:
        number_of_days_int = int(number_of_days_dict["day"])
        if number_of_days_int > 0:
            conversion = convert_function(number_of_days_int, number_of_days_dict["unit"])
            print(conversion)
        elif number_of_days_int == 0:
            print("You entered 0, not a correct value")
        else:
            print("You entered a negative value")
    except ValueError:
        print("Enter a correct value")
