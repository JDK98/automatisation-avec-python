from helper import validation

number_of_days = ""
while number_of_days != "quit":
    number_of_days = input("Combien de jours et en quelle unité souhaitez vous convertir : \n")
    days_units = number_of_days.split(":")
    number_of_days_dict = {"day": days_units[0], "unit": days_units[1]}
    validation(number_of_days_dict)
