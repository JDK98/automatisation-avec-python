import requests
import hashlib
import sys

def request_api(query_char):
    url = "https://api.pwnedpasswords.com/range/" + query_char
    res = requests.get(url)
    if res.status_code != 200:
        raise RuntimeError(f"Erreur {res.status_code} \nVérifiez l'API et réessayez")
    return res

def get_password_leaks_count(hashes, hash_to_check):
    hashes = (line.split(":") for line in hashes.text.splitlines())
    for h, count in hashes:
        if h == hash_to_check:
            return count
    return 0

def pwned_api_check(password):
    sha1password = hashlib.sha1(password.encode('utf-8')).hexdigest().upper()
    first5_character, tail = sha1password[:5], sha1password[5:]
    response = request_api(first5_character)
    return get_password_leaks_count(response, tail)

def main(args):
    for password in args:
        count = pwned_api_check(password)
        if count:
            print(f"🚨 {password} a été hacké {count} fois... Vous devriez changez de mot de passe !")
        else:
            print(f"✅  {password} n'a jamais été piraté... Vous devriez le conserver !")

if __name__ == "__main__":
main(sys.argv[1:])
