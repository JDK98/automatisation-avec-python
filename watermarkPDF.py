import PyPDF2

pdf = PyPDF2.PdfFileReader(open("super.pdf", "rb"))
watermark = PyPDF2.PdfFileReader(open("pdfFile/wtr.pdf", "rb"))
output = PyPDF2.PdfFileWriter()

for i in range(pdf.getNumPages()):
    page = pdf.getPage(i)
    page.mergePage(watermark.getPage(0))
    output.addPage(page)

    with open("watermarked_page.pdf", "wb") as result:
        output.write(result)

