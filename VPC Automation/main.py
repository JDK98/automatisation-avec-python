import boto3

ec2_resource = boto3.resource('ec2', region_name="eu-west-1")

new_vpc = ec2_resource.create_vpc(
    CidrBlock='172.32.0.0/16'
)
new_vpc.create_subnet(
    CidrBlock='172.32.1.0/24'
)
new_vpc.create_subnet(
    CidrBlock='172.32.2.0/24'
)
new_vpc.create_tags(
    Tags=[
        {
            'Key': 'Name',
            'Value': 'Python-vpc'
        },
    ]
)

ec2_client = boto3.client('ec2')
all_available_vpcs = ec2_client.describe_vpcs()
last_vpc = all_available_vpcs["Vpcs"][-1]
vpcs = all_available_vpcs["Vpcs"]
print(f"Your Newly Created VPC is Cidr: {last_vpc['CidrBlock']}, VpcId: {last_vpc['VpcId']}")

for vpc in vpcs:
    print(f"Cidr: {vpc['CidrBlock']}, VpcId: {vpc['VpcId']}, Nested_Cidr: {vpc['CidrBlockAssociationSet'][0]['CidrBlock']}")

