import sys
import os
from PIL import Image

# Grab first and second argument
source_dir = sys.argv[1]
dest_dir = sys.argv[2]

# Check if dest_dir exist, else create
if not os.path.exists(dest_dir):
    os.mkdir(dest_dir)

for filename in os.listdir(source_dir):
    img = Image.open(source_dir + filename)
    clean_name = os.path.splitext(filename)[0]
    img.save(f"{dest_dir}{clean_name}.png", "png")
