from translate import Translator
translator = Translator(to_lang="ja")

try:
    with open("test.txt", mode="r") as myFile:
        traduction = translator.translate(myFile.read())
        print(traduction)
except FileNotFoundError as err:
    print ("There is an error")
    raise
