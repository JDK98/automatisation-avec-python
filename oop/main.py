from user import User
from post import Post

User1 = User("jd@gmail.com", "jdk", "text", "DevOps")
User1.get_user_info()

User2 = User("moon@gmail.com", "moon", "tech", "Ops")
User2.get_user_info()

Post1 = Post("Practice makes perfect", User1.name)
Post1.get_post_info()


