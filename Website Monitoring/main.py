import requests
import smtplib
import os
import paramiko
import time
import schedule
import boto3

ec2 = boto3.client('ec2', region_name='eu-west-1')
instances = ["i-07fb48530ddd2692d"]

# Variable definit dans PyCharm ou en persistence sur le terminal
EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PWD = os.environ.get('EMAIL_PWD')

def send_notification(email_msg):
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PWD)
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, email_msg)

def restart_application():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname="54.74.109.95", username="ubuntu", key_filename="/Users/JDK/.ssh/magic-key.pem")
    stdin, stdout, stderr = ssh.exec_command("sudo docker container restart nginx")
    ssh.close()

def monitor_application():
    try:
        response = requests.get('http://ec2-54-74-109-95.eu-west-1.compute.amazonaws.com:8080/')
        if response.status_code == 200:
            print("✅ Application is running")
        else:
            print("🚨 APPLICATION DOWN")
            msg = f"Subject: WEBSITE DOWN \n Application returned error : {response.status_code} !"
            print(f"👉🏼 Sending the notifications to {EMAIL_ADDRESS}")
            send_notification(msg)
            print("✅ Notifications successfully sent")

            # reboot the server
            print("👉🏼 Trying to debug by Rebooting the server")
            ec2.reboot_instances(InstanceIds=instances)
            print("✅ Server successfully rebooted")

            # Waiting for server to reboot
            print("👉🏼 Waiting for server to reboot")
            time.sleep(150)
            print("✅ The program can continue")

            # restart the application
            print("👉🏼 Trying to debug by restarting the application")
            restart_application()
            print("✅ Application restarted successfully")

    except Exception as ex:
        print(f"Erreur de connexion: {ex}")
        msg = f"Subject: Connexion Error \n Application not accessible !"
        send_notification(msg)

schedule.every(15).minutes.do(monitor_application)
while True:
    schedule.run_pending()

