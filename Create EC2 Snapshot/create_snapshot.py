import boto3
import schedule

ec2_client = boto3.client('ec2', region_name='eu-west-1')
ec2_resource = boto3.resource('ec2', region_name='eu-west-1')

def create_snapshot():
    volumes = ec2_client.describe_volumes()['Volumes']
    for volume in volumes:
        new_snapshot = ec2_client.create_snapshot(
            VolumeId=volume['VolumeId']
        )
        print(new_snapshot)

schedule.every().day.do(create_snapshot)
while True:
    schedule.run_pending()


