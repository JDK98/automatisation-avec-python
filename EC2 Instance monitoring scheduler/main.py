import boto3
import schedule

ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')

def check_instance_status():
    states = ec2_client.describe_instance_status(
        IncludeAllInstances=True
    )
    for state in states['InstanceStatuses']:
        status = state['InstanceState']['Name']
        ins_state = state['InstanceStatus']['Status']
        sys_state = state['SystemStatus']['Status']
        print(f"Instance {state['InstanceId']} est {status} avec un statut instance {ins_state} et un statut système {sys_state}")
    print("#############################\n")

schedule.every(5).minutes.do(check_instance_status)
while True:
    schedule.run_pending()




